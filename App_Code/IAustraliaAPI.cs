﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

public class Entity
{
    public string ID { get; set; }
}

[ServiceContract]
public interface IAustraliaAPI
{
    [OperationContract]
    [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json, 
        BodyStyle = WebMessageBodyStyle.Bare, 
        UriTemplate = "data/{id}")]
    Entity GetData(string id);
}